Name:
	bam2bedgraph

Version:
	1.4 (2016-04-13)
	
Synopsis:
	bam2bedgraph -s -o OUTDIR -i mySample.sorted.bam
	
	-or-
	
	bam2bedgraph -s -w BATCHNAME -o OUTDIR -b batchfile.tsv

Description:
	This scripts takes a sorted bam file, or a set of 
	them and runs bedtools genomecov and some additional
	PERLscript to determine the coverage,
	normalize the data (opt),
	log(N+1) transform the data (opt), 
	sort the data, and preappend a header line.
	The resulting bedgraph file is ready
	for uploading to a UCSC genome browser.

	You can either run one file at a time with the -i option,
	or run it in batch mode with the -b option (see below).
	
	Input files must be a sorted bam file
	and the filename(s) should be of the format:
		yourSampleName.sorted.bam

	Starting with a sam file? Run:
		samtools view -b yourSample.sam > yourSample.bam 
		samtools sort -o yourSample.sorted.bam yourSample.bam

	The script assumes that you have 
	bedtools (v2.25.0 or equivalent) and 
	samtools (v1.3 or equivalent) 
	in your $PATH.

Options:
	-h		
			Print this help file.

	-i YOURSAMPLEFILE.sorted.bam	
			Use to specify the name of your input file when
			you run the script on a single bam file. Otherwise, 
			use the batch option below.

	-b YOURBATCHFILE.tsv
			For batch processing. 
			Specify the name of your batchfile here. 
			It should be tab-delimited text file 
		 	formated as described below. 

	-s		
			Use for stranded data. [default = unstranded]
			This will make two files (for/rev)
			that can be overlapped on the browser.

	-p		
			For Paired End RNAseq data. [default = single end]
			Use with the -s parameter.

	-n auto|none|<integer>		
			A normalization factor. [default = auto]
			By default, all coverage values are normalized
			by the genome size / total coverage.
			Alternatively, you can specify an integer, and
			all coverage values will be multiplied by this factor.
			Use 1 or none for no normalization.
			Individual normalization factors can also be specified 
			in a batch file with the -b option.
	
	-l 
			Linear scale. Add this flag to disable
			log(N+1) normalization of the data.

	-w BATCHNAME		
			Use this flag to generate a summary file 
			and a tsv file of track headers for
			the UCSC webmaster. Specify the batch name 
			either in quotes or without spaces here.

 	-o OUTDIR
 			Specify an output directory 
			[default = current directory]
			The script will create the directory 
			if it does not exist.

Batch Mode:
	Create a tab delimited text file to specify the input parameters. 
	It should 5 columns for each bam file, each separated by tabs, 
	as follows:

	1. sampleName.sorted.bam	# The name of your sorted file.

	2. shortSampleName 		# This will be the short track name. 
					# Up to 15 alpha-numeric characters.
					# No spaces.

	3. the long sample name		# This will be the long track name. 
					# Spaces are okay here.

	4. the normalization factor	# Can be auto or none or an integer.

	5. an RBG color			# Specify an RGB color (e.g 255,90,0)
					# or auto to autogenerate color. 

