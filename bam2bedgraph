#!/usr/bin/perl

use warnings;
use strict;
use Cwd ;
use Cwd 'abs_path';


# This is a perl script to convert a
# sorted bam file of RNAseq alignments into
# a bedGraph file for loading on 
# a UCSC genome browser. It normalizes and
# log(N+1) transforms the data. 
# Additonally, it generates a 
# header line with a random RGB track color.
# It expects that bedtools v2.25.0 or
# equivalent and samtools 
# are in your $PATH
#
# author: Sean D. Gallaher
# date: 13-APR-2016
# version: v1.4
# file: bam2bedgraph



# The help:
my $error = "\nInput not recognized!\nAdd -h to view the detailed help\n\n";
my $noHelp = "\nThe help file is missing. You can find the manual at\nhttps://bitbucket.org/gallaher/bam2bedgraph\n\n";
my $script = $0;
my $scriptAbsPath = abs_path ($script) ;
$scriptAbsPath =~ m/(.+)\/[^\/]+$/;
my $pathToScript = $1;
my $helpFile = $pathToScript . "/README.txt";


# Set the workdir
my $workdir = getcwd;


# Take user inputs:

my $stranded = "false"; 	# default is unstranded
my $norm = "auto"; 		# default normalization factor
my $logTransform = "true";	# default is log(N+1) transform data
my $paired = "false" ;		# default is single end reads
my $file;
my %sampleTab;
my $fail = "true"; 		# die if neither -i or -b is set
my $groupName = "undef";

my $numFlags = @ARGV;

if ($numFlags == 0) {
	die "$error";
}

for (my $i = 0; $i<$numFlags ; $i++)  {
	my $flag = $ARGV[$i];
	if ($flag eq "-h") {
		open (HELP, "<", $helpFile)
			or die "$noHelp";
		print STDOUT "\n";
		while (my $line = <HELP>) {
			print STDOUT "$line";
		}
		close (HELP);
		die "\n";
	}
	elsif ($flag eq "-i") {
		$i++;
		$file = $ARGV[$i];
		$file =~ m/([^\/]+)\.sorted\.bam$/;
		my $root = $1;
		$root =~ s/ /_/g;
		$sampleTab{$file}{'short'} = $root;
		$sampleTab{$file}{'long'} = "insert description here";
		my $rgb = &color;
		$sampleTab{$file}{'RGB'} = $rgb;
		$sampleTab{$file}{'norm'} = $norm;
		$fail = "false";
	}	
	elsif ($flag eq "-l") {
		$logTransform = "false";
	}
	elsif ($flag eq "-s") {
		$stranded = "true";
	}
	elsif ($flag eq "-p") {
		$paired = "true" ;
	}
	elsif ($flag eq "-n") {
		$i++;
		my $noNorm = "\nThe normalization factor is not recognized.\nThe options are (auto|none|<integer>).\nUse -h to see detailed help.\n\n";
		my $normOption = $ARGV[$i];
		if (!defined $normOption) {
			die "$noNorm";
		}
		if ($normOption =~ m/^(a|A)/) {
			$norm = "auto";
		}
		elsif ($normOption =~ m/^(n|N)/) {
			$norm = 1;
		}
		elsif ($normOption =~ m/(\d+)/) {
			$norm = $1;
		}
		else {
			die "$noNorm";
		}
		if (defined $file) {
			$sampleTab{$file}{'norm'} = $norm;
		}
	}
	elsif ($flag eq "-w") {
		$i++;
		$groupName = $ARGV[$i];
		if ($groupName =~ m/^-/) {
			$groupName = "NoGroupName";
			$i--;
		}
	}
	elsif ($flag eq "-o") {
		$i++;
		$workdir = $ARGV[$i];
		if (!-d "$workdir") {
			mkdir "$workdir";
		}
	}
	elsif ($flag eq "-b") {
		$i++;
		my $batchTab = $ARGV[$i];
		open (BATCH , "<", $batchTab) or die "Cannot open $batchTab: $!\n";
		while (my $line = <BATCH>) {
			chomp $line;
			if ($line =~ m/^$/) {
				next;
			}
			my @lineArray = split (/\t/, $line);
			my $cols = @lineArray;
			unless ($cols >= 4 ) {
				die "\nThere is a problem with your batch table!\n\nType bam2bedgraph -h for detailed help\n\n";
			}
			my $file = $lineArray[0];
			$file =~ m/([^\/]+)\.sorted\.bam$/;
			my $root = $lineArray[1];
			$root =~ s/ /_/g;
			$sampleTab{$file}{'short'}=$root;
			$sampleTab{$file}{'long'}=$lineArray[2];

			$sampleTab{$file}{'norm'}=$lineArray[3];
			my $tabNorm = $lineArray[3];
			if ($tabNorm =~ m/^(n|N)/) {			
				$sampleTab{$file}{'norm'} = 1;
			}
			elsif ($tabNorm =~ m/(\d+)/) {
				$sampleTab{$file}{'norm'} = $1;
			}
			else {
				$sampleTab{$file}{'norm'} = "auto";
			}
			if (!defined $lineArray[4]) {
				my $rgb = &color;
				$sampleTab{$file}{'RGB'}=$rgb;
			}
			elsif ($lineArray[4] =~ m/^(A|a)/) {
				my $rgb = &color;
				$sampleTab{$file}{'RGB'}=$rgb;
			}
			else {
				$sampleTab{$file}{'RGB'}=$lineArray[4];
			}
		}
		close (BATCH);
		$fail = "false";
	}
}

if ($fail eq "true") {
	print "\nUser input not recognized.\n\n";
	die "$error";
}


my @summary;
my $sumRef = \@summary;
my @headers;
my $headRef = \@headers;

foreach my $file (sort keys %sampleTab) {
	my $short = $sampleTab{$file}{'short'};
	my $long = $sampleTab{$file}{'long'};
	my $rgb = $sampleTab{$file}{'RGB'};
	my $norm = $sampleTab{$file}{'norm'};
	my $genomeSize = &genome($file);
	&runBedtools($file,$short,$long,$norm,$genomeSize,$rgb,$stranded,$logTransform,$paired,$groupName,$sumRef,$headRef);
}

if ($groupName ne "undef") {
	my $sumFile = "$workdir/$groupName" . ".summary.tsv";
	my $headerFile = "$workdir/$groupName" . ".headers.tsv";
	open (HEAD, ">", $headerFile) 
		or die "Cannot open $headerFile: $!\n";
	open (SUM, ">", $sumFile) 
		or die "Cannot open $sumFile: $!\n";
	foreach my $line (@summary) {
		print SUM "$line\n";
	}
	foreach my $line (@headers) {
		print HEAD "$line\n";
	}
	close (SUM);
	close (HEAD);
}


###############
# subroutines #
###############

sub runBedtools {
	# This subroutine runs bedtools for each file, creates a temp.bedgraph
	# and creates the header line. Then it runs &normAndTransform 
	# on each file in turn

	
	my $file = shift @_;
	my $short = shift @_;
	my $long = shift @_;
	my $norm = shift @_;
	my $genomeSize = shift @_;
	my $rgb = shift @_;
	my $stranded = shift @_;
	my $logTransform = shift @_;
	my $paired = shift @_;
	my $groupName = shift @_;
	my $sumRef = shift @_;
	my $headRef = shift @_;
	

	# Here are the commands to run bedtools genomecov
	my $runBedtoolsBoth = qq{bedtools genomecov -bg -ibam $file -split  > $workdir/temp.$short.bedgraph };
	my $runBedtoolsFor = qq{bedtools genomecov -bg -ibam $file -strand + -split > $workdir/temp.$short.for.bedgraph };
	my $runBedtoolsRev = qq{bedtools genomecov -bg -ibam $file -strand - -split > $workdir/temp.$short.rev.bedgraph };
	my $runBedtoolsForPE = qq{bedtools genomecov -bg -ibam for.bam -strand + -split > $workdir/temp.$short.for.bedgraph };
	my $runBedtoolsRevPE = qq{bedtools genomecov -bg -ibam rev.bam -strand - -split > $workdir/temp.$short.rev.bedgraph };



	if ($stranded eq "false") {
		system $runBedtoolsBoth ;
		my $tempBoth = "$workdir/temp.$short.bedgraph";
		my $header =  "track\ttype=bedGraph\tname=$short\tdescription=\"$long\"\tvisibility=full\tcolor=$rgb\tviewLimits=0:3\tmaxHeightPixels=32\tyLineOnOff=on\tautoScale=off\n";
		my $strand = "both";
		my $summaryLine = "$short.bedgraph\t$short\t\"$long\"\t\"$groupName\"\t$rgb";
		push (@{$sumRef} , $summaryLine);
		push (@{$headRef} , $header);
		&normAndTransform ($tempBoth,$header,$strand,$norm,$logTransform,$genomeSize);
	}
	elsif ($stranded eq "true") {
		if ($paired eq "true") {
			&splitPairs($file) ;
			system $runBedtoolsForPE ;
			system $runBedtoolsRevPE ;
			system qq{rm for.bam};
			system qq{rm rev.bam};

		}
		else {
			system $runBedtoolsFor ;
			system $runBedtoolsRev ;
		}
		my $tempFor = "$workdir/temp.$short.for.bedgraph";
		my $header =  "track\ttype=bedGraph\tname=$short" . "_F\tdescription=\"$long forward strand\"\tvisibility=full\tcolor=$rgb\tviewLimits=0:3\tmaxHeightPixels=32\tyLineOnOff=on\tautoScale=off\n";
		my $strand = "for";
		my $summaryLine = "$short.for.bedgraph\t$short" . "_F\t\"$long forward strand\"\t\"$groupName\"\t$rgb";
		push (@{$sumRef} , $summaryLine);
		push (@{$headRef} , $header);
		&normAndTransform ($tempFor,$header,$strand,$norm,$logTransform,$genomeSize);
		my $tempRev = "$workdir/temp.$short.rev.bedgraph";
		my $revHeader =  "track\ttype=bedGraph\tname=$short" . "_R\tdescription=\"$long reverse strand\"\tvisibility=full\tcolor=$rgb\tviewLimits=-3:0\tmaxHeightPixels=32\tyLineOnOff=on\tautoScale=off\n";
		$strand = "rev";
		$summaryLine = "$short.rev.bedgraph\t$short" . "_R\t\"$long reverse strand\"\t\"$groupName\"\t$rgb";
		push (@{$sumRef} , $summaryLine);
		push (@{$headRef} , $header);
		&normAndTransform ($tempRev,$revHeader,$strand,$norm,$logTransform,$genomeSize);
	}
}


sub normAndTransform {
	# this subroutine takes four inputs:
	# 1) the temp.bedgraph file
	# 2) the header
	# 3) the strand
	# 4) the normalization factor or "auto"
	# 5) the genome size
	# First it preappends the header. Then, it
	# multiplies the coverage by the normalization
	# factor, and then log(N+1) transforms it
	# before writing the result to a new bedgraph file 

	my $raw = shift @_;
	my $header = shift @_;
	my $strand = shift @_;
	my $norm = shift @_;
	my $logTransform = shift @_;
	my $genomeSize = shift @_;
	my $totalCov;
	my $normFactor;

	$raw =~ m/(.*)temp\.(.+)/;
	my $sorted = "$1.temp.sorted$2";
	my $output = "$1$2";

	system qq{sort -k1,1 -k2,2n $raw > $sorted};
	system qq{rm $raw};


	# Automatically determine the normalization factor.
	if ($norm eq "auto") {
		open (IN, "<", $sorted) 
			or die "Cannot open $sorted: $!\n";
		while (my $line = <IN>) {
			chomp $line;
			my @lineArray = split (/\t/, $line);
			my $cov = $lineArray[3];
			$totalCov += $cov;
		}
		close (IN);
		$normFactor = $genomeSize / $totalCov;
	}
	else {
		$normFactor = $norm;
	}

	open (IN, "<", $sorted)
		or die "Cannot open $sorted: $!\n";
	open (OUT , ">", $output)
		or die "Cannot open $output: $!\n";

	print OUT "$header";

	while (my $line = <IN>) {
		chomp $line;
		my @lineArray = split (/\t/, $line);
		my $cov = $lineArray[3];
		my $normCov = $cov * $normFactor;
		my $rndCov;
		if ($logTransform eq "true") {
			my $logCov = &log10 ($normCov);
			$rndCov = sprintf("%.4f" , $logCov);
		}
		elsif ($logTransform eq "false") {
			$rndCov = sprintf("%.4f" , $normCov);
		}
		if ($strand eq "rev") {
			my $negCov = -$rndCov;
			$lineArray[3] = $negCov;
		}
		else {
			$lineArray[3] = $rndCov;
		}
		my $final = join ("\t", @lineArray);
		print OUT "$final\n";
	}
	
	close (IN);
	close (OUT);
	system ("rm $sorted");	
}


sub log10 {
	my $n = shift @_;
 	$n++;
  	return log($n)/log(10);
}

sub color {
        my $r = int(rand(256));
        my $g = int(rand(256));
        my $b = int(rand(256));
        my $sum = $r + $b + $g;
        if ($sum > 500) {
                if ($r > 200) {
                        $r = int(rand(150));
                }
                elsif ($g > 200) {
                        $g = int(rand(150));
                }
                elsif ($b > 200) {
                        $b = int(rand(150));
                }
        }
        if ($sum > 500) {
                if ($g > 200) {
                        $g = int(rand(150));
                }
                elsif ($b > 200) {
                        $b = int(rand(150));
                }
                elsif ($r > 200) {
                        $r = int(rand(150));
                }
        }
        my $rgb = $r . "," . $g . "," .  $b;
        return $rgb;
}

sub genome {
	my $temp = "$workdir/temp.file.tsv";
	my $bam = shift @_;
	my $runSamtools = qq{samtools view -H $bam | grep SQ | cut -f 3 | sed -E "s/LN://" > $temp};

	system ("$runSamtools");

	open (TEMP, "<", $temp)
		 or die "Script failed to calculate genome size from\nBAM file. Is samtools in your path?\nCould not open $temp: $!\n";

	my $sum;

	while (my $line = <TEMP>) {
        	chomp $line;
        	$sum += $line;
	}


	close (TEMP);
	system ("rm $temp");
	return $sum ;
}

sub splitPairs {
	# This is adapted from a bash script written by
	# Istvan Albert and posted to 	
	# https://www.biostars.org/p/92935/

	my $file = shift @_;
	system qq{samtools view -b -f 128 -F 16 $file > for1.bam };
	system qq{samtools view -b -f 80 $file > for2.bam };
	system qq{samtools merge -f for.bam for1.bam for2.bam};
	system qq{samtools view -b -f 144 $file > rev1.bam};
	system qq{samtools view -b -f 64 -F 16 $file > rev2.bam};
	system qq{samtools merge -f rev.bam rev1.bam rev2.bam};
	system qq{rm for1.bam};
	system qq{rm for2.bam};
	system qq{rm rev1.bam};
	system qq{rm rev2.bam};
}

